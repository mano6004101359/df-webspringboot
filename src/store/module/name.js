const state = {
  list: {
    index: null,
    comname: "",
    cpu: "",
    mainb: "",
    ram: "",
    vga: ""
  },
  nameList: []
};
const getters = {
  stateName: state => {
    return state;
  }
};
const mutations = {
  ADD(state, index) {
    if (index.index != null) {
      state.nameList[index.index].comname = index.comname;
      state.nameList[index.index].cpu = index.cpu;
      state.nameList[index.index].mainb = index.mainb;
      state.nameList[index.index].ram = index.ram;
      state.nameList[index.index].vga = index.vga;
    } else {
      state.nameList.push(index);
    }
  },
  DELETENAME(state, { index }) {
    state.nameList.splice(index, 1);
  },
  EDIT(state, index) {
    const data = state.nameList[index];

    state.list.index = index;
    state.list.comname = data.comname;
    state.list.cpu = data.cpu;
    state.list.mainb = data.mainb;
    state.list.ram = data.ram;
    state.list.vga = data.vga;

    console.log("array", state.nameList[index]);
    console.log("array", index);
  },
  RESET(state) {
    state.list = {
      index: null,
      comname: null,
      cpu: null,
      mainb: null,
      ram: null,
      vga: null,
    };
  }
};
const actions = {
  add: ({ commit }, payload) => {
    commit("ADD", payload);
  },
  deleteName: ({ commit }, index) => {
    commit("DELETENAME", index);
  },
  edit: ({ commit }, index) => {
    commit("EDIT", index);
  },
  reset: ({ commit }) => {
    commit("RESET");
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
