import Vue from "vue";
import Vuex from "vuex";
import name from "../store/module/name";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: { name }
});
